# docker-compose-openvas

This repo has a docker-compose file that you can use to spin up OpenVAS

It builds a persistent volume that the OpenVAS container writes to. So you can freely shutdown or restart the container at anytime.

### TODO
Need to allow docker to run on more than one CPU core
